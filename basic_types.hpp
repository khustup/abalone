#ifndef BASIC_TYPES_HPP
#define BASIC_TYPES_HPP

#include <cassert>
#include <iostream>
#include <utility>

/// @brief Color of marbles.
enum color {
    BLACK_COLOR,
    WHITE_COLOR
};

/// @brief Row coordinates
enum letter {
    A = 1,
    B = 2,
    C = 3,
    D = 4,
    E = 5,
    F = 6,
    G = 7,
    H = 8,
    I = 9
};

/// @brief Column coordinates.
typedef int number;

/// @brief Coordinate type, pair of row and column.
typedef std::pair<letter, int> coordinate;

/// @brief Direction for move.
enum direction {
    NORTH_EAST,
    NORTH_WEST,
    WEST,
    SOUTH_WEST,
    SOUTH_EAST,
    EAST
};

enum move_type {
    INLINE,
    BROADSIDE
};

/**
 * @brief Move, pair of coordinate and direction.
 */
class move {
public:
    inline move(move_type t = INLINE, coordinate c = coordinate(A, 1), direction d = NORTH_WEST, int m = 0)
        : m_type(t)
        , m_coordinate(c)
        , m_direction(d)
        , m_marbles_count(m)
    {
        assert(t == INLINE || m == 2 || m == 3);
    }

    inline move_type get_move_type() const
    {
        return m_type;
    }

    inline coordinate get_coordinate() const
    {
        return m_coordinate;
    }

    inline direction get_direction() const
    {
        return m_direction;
    }

    inline int get_marbles_count() const
    {
        assert (m_type == BROADSIDE);
        return m_marbles_count;
    }

private:
    friend std::ostream& operator<< (std::ostream&, const move&);
    friend std::istream& operator>> (std::istream&, move&);

private:
    move_type m_type;
    coordinate m_coordinate;
    direction m_direction;
    int m_marbles_count;
};

inline std::ostream& operator<< (std::ostream& s, const coordinate& m)
{
    s << static_cast<int>(m.first) << '\n';
    s << m.second << '\n';
    return s;
}

inline std::istream& operator>> (std::istream& s, coordinate& m)
{
    int x;
    s >> x;
    m.first = static_cast<letter>(x);
    s >> m.second;
    return s;
}

inline std::ostream& operator<< (std::ostream& s, const move& m)
{
    s << static_cast<int>(m.m_type) << '\n';
    s << m.m_coordinate;
    s << static_cast<int>(m.m_direction) << '\n';
    s << m.m_marbles_count << '\n';
    return s;
}

inline std::istream& operator>> (std::istream& s, move& m)
{
    int x;
    s >> x;
    m.m_type = static_cast<move_type>(x);
    s >> m.m_coordinate;
    s >> x;
    m.m_direction = static_cast<direction>(x);
    s >> m.m_marbles_count;
    return s;
}

#endif
