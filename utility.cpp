#include "utility.hpp"

#include <cassert>
#include <cmath>

bool check_coordinate(coordinate c)
{
    if (c.second < 1) {
        return false;
    }
    return c.second >= get_start_coordinate(c.first) && c.second <= get_end_coordinate(c.first);
}

int get_count_in_row(int c)
{
    return 9 - std::abs(5 - c);
}

number get_start_coordinate(letter l)
{
    return l <= E ? 1 : l - E + 1;
}

number get_end_coordinate(letter l)
{
    return l >= E ? 9 : 8 - (D - l);
}

bool is_end(coordinate c, direction d)
{
    switch (d) {
        case NORTH_EAST:
            if (c.first == I || c.second == 9 ) {
                return true;
            }
            break;
        case NORTH_WEST:
            if (c.first == I || (c.first >= E && c.second == get_start_coordinate(c.first))) {
                return true;
            }
            break;
        case WEST:
            if (c.second == get_start_coordinate(c.first)) {
                return true;
            }
            break;
        case SOUTH_WEST:
            if (c.first == A || c.second == 1) {
                return true;
            }
            break;
        case SOUTH_EAST:
            if (c.first == A || (c.first <= E && c.second == get_end_coordinate(c.first))) {
                return true;
            }
            break;
        case EAST:
            if (c.second == get_end_coordinate(c.first)) {
                return true;
            }
            break;
        default:
            assert(!"invalid direction!");
    }
    return false;
}

coordinate get_neighbour(coordinate c, direction d)
{
    assert(!is_end(c, d));
    int x = static_cast<int>(c.first);
    switch (d) {
        case NORTH_EAST:
            return coordinate(static_cast<letter>(x + 1), c.second + 1);
        case NORTH_WEST:
            return coordinate(static_cast<letter>(x + 1), c.second);
        case WEST:
            return coordinate(c.first, c.second - 1);
        case SOUTH_WEST:
            return coordinate(static_cast<letter>(x - 1), c.second - 1);
        case SOUTH_EAST:
            return coordinate(static_cast<letter>(x - 1), c.second);
        case EAST:
            return coordinate(c.first, c.second + 1);
        default:
            assert(!"invalid direction!");
    }
    return coordinate(A, -1);
}

direction get_broadside_direction(direction d)
{
    return static_cast<direction>((static_cast<int>(d) + 5) % 6);
}

direction get_opposite_direction(direction d)
{
    return static_cast<direction>((static_cast<int>(d) + 3) % 6);
}
