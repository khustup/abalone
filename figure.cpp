#include "figure.hpp"
#include "utility.hpp"

#include <cassert>

figure::figure(color c, coordinate cc)
    : m_color(c)
    , m_coordinate(cc)
{
}

color figure::get_color() const
{
    return m_color;
}

coordinate figure::get_coordinate() const
{
    return m_coordinate;
}

void figure::set_coordinate(coordinate cc)
{
    assert(check_coordinate(cc));
    m_coordinate = cc;
}
