#include "main_window.hpp"
#include "engine.hpp"

#include <QApplication>

#include <ctime>
#include <cstdlib>

int main(int argc, char** argv)
{
    srand(time(0));
    QApplication app(argc, argv);
    engine e;
    e.get_main_window().show();
    return app.exec();
}
