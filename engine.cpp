#include "engine.hpp"
#include "main_window.hpp"

#include <cassert>
#include <fstream>

engine::engine()
    : m_main_window(new main_window(this))
    , m_max_id(0)
    , m_timer(this)
    , m_level(PUSH_AND_DESTROY)
{
    QObject::connect(this, SIGNAL(game_updated(int)), m_main_window, SLOT(update_game(int)));
    QObject::connect(this, SIGNAL(game_finished(int)), m_main_window, SLOT(game_finished(int)));
    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(update_games()));
    m_timer.start(1000);
}

engine::~engine()
{
    std::map<int, board*>::iterator i = m_games.begin();
    while (i != m_games.end()) {
        delete (i++)->second;
    }
    m_games.clear();
    delete m_main_window;
    m_main_window = 0;
}

main_window& engine::get_main_window()
{
    assert(m_main_window != 0);
    return *m_main_window;
}

int engine::create_new_game()
{
    m_games.insert(std::make_pair(m_max_id, new board()));
    emit game_updated(m_max_id);
    return m_max_id++;
}

bool engine::game_exists(int id) const
{
    return m_games.find(id) != m_games.end();
}

const board& engine::get_board(int id) const
{
    assert(game_exists(id));
    return *m_games.find(id)->second;
}

board& engine::get_board(int id)
{
    assert(game_exists(id));
    return *m_games.find(id)->second;
}

void engine::pause(int id)
{
    assert(id >= 0);
    m_stopped_games.insert(id);
}

void engine::resume(int id)
{
    assert(id >= 0);
    m_stopped_games.erase(id);
}

void engine::undo(int id)
{
    if (game_exists(id)) {
        board& b = get_board(id);
        if (b.has_undo_move()) {
            b.undo();
            emit game_updated(id);
            pause(id);
        }
    }
}

void engine::redo(int id)
{
    if (game_exists(id)) {
        board& b = get_board(id);
        if (b.has_redo_move()) {
            b.redo();
            emit game_updated(id);
        }
    }
}

bool engine::is_game_stopped(int id) const
{
    assert(id >= 0);
    return m_stopped_games.find(id) != m_stopped_games.end();
}

void engine::save(int id, std::string path) const
{
    if (game_exists(id)) {
        std::ofstream f(path.c_str());
        const board& b = get_board(id);
        const std::vector<move>& h = b.get_history();
        std::vector<move>::const_iterator i = h.begin();
        while (i != h.end()) {
            f << *i++;
        }
    }
}

int engine::load(std::string path)
{
    std::ifstream f(path.c_str());
    board* b = new board();
    move m;
    bool c = false;
    while (f >> m) {
        if (b->move_is_correct(m)) {
            c = true;
            b->make_move(m);
        } else {
            c = false;
            break;
        }
    }
    if (c) {
        m_games.insert(std::make_pair(m_max_id, b));
        emit game_updated(m_max_id);
        return m_max_id++;
    }
    delete b;
    return -1;
}

engine::level engine::get_level() const
{
    return m_level;
}

void engine::set_level(level l)
{
    m_level = l;
}

std::vector<int> engine::get_games() const
{
    std::vector<int> r;
    std::map<int, board*>::const_iterator i = m_games.begin();
    while (i != m_games.end()) {
        r.push_back(i->first);
        ++i;
    }
    return r;
}

void engine::update_games()
{
    std::map<int, board*>::iterator i = m_games.begin();
    while(i != m_games.end()) {
        if(!i->second->is_finished() && !is_game_stopped(i->first)) {
            make_random_move(*(i->second));
            emit game_updated(i->first);
            if (i->second->is_finished()) {
                emit game_finished(i->first);
            }
        }
        ++i;
    }
}

void engine::make_random_move(board& b)
{
    assert(!b.is_finished());
    color c = b.get_turn();
    std::vector<move> pd_moves;
    std::vector<move> p_moves;
    std::vector<move> r_moves;
    std::vector<coordinate> v = b.get_figures(c);
    for (unsigned i = 0; i < v.size(); ++i) {
        for (int j = NORTH_EAST; j <= EAST; ++j) {
            switch (m_level) {
            case PUSH_AND_DESTROY:
                if (b.move_pushes_and_destroys(move(INLINE, v[i], static_cast<direction>(j)))) {
                    pd_moves.push_back(move(INLINE, v[i], static_cast<direction>(j)));
                    break;
                }
            case PUSH:
                if (b.move_pushes(move(INLINE, v[i], static_cast<direction>(j)))) {
                    p_moves.push_back(move(INLINE, v[i], static_cast<direction>(j)));
                    break;
                }
            case RANDOM:
                if (b.move_is_correct(move(INLINE, v[i], static_cast<direction>(j)))) {
                    r_moves.push_back(move(INLINE, v[i], static_cast<direction>(j)));
                }
                if (b.move_is_correct(move(BROADSIDE, v[i], static_cast<direction>(j), 2))) {
                    r_moves.push_back(move(BROADSIDE, v[i], static_cast<direction>(j), 2));
                }
                if (b.move_is_correct(move(BROADSIDE, v[i], static_cast<direction>(j), 3))) {
                    r_moves.push_back(move(BROADSIDE, v[i], static_cast<direction>(j), 3));
                }
                break;
            default:
                assert(!"invalid level");
            }
        }
    }
    if (!pd_moves.empty()) {
        int x = rand() % pd_moves.size();
        b.make_move(pd_moves[x]);
    } else if (!p_moves.empty()) {
        int x = rand() % p_moves.size();
        b.make_move(p_moves[x]);
    } else if (!r_moves.empty()) {
        int x = rand() % r_moves.size();
        b.make_move(r_moves[x]);
    }
}
