#ifndef FIGURE_HPP
#define FIGURE_HPP

#include "basic_types.hpp"

class figure
{
public:
    figure(color c, coordinate cc);
    
    color get_color() const;

    coordinate get_coordinate() const;

    void set_coordinate(coordinate cc);

private:
    figure(const figure&);
    figure& operator=(const figure&);

    color m_color;
    coordinate m_coordinate;
};

#endif
