#ifndef BOARD_HPP
#define BOARD_HPP

#include "basic_types.hpp"
#include "figure.hpp"

#include <vector>

/**
 * @class board.
 * @brief Class representing game board.
 *
 *        board represents the Abalone game board. It keeps the position of 
 *        marbles and is responsible for doing moves and checking the rules
 *        of game for each move.
 */
class board
{
public:
    /**
     * @brief Constructor. Initializes the board.
     */
    board();

    /**
     * @brief Destructor.
     */
    ~board();

    /**
     * @brief Checks whether there is a figure on the given coordinate.
     */
    bool figure_exists(coordinate c) const;

    /**
     * @brief Returns the color of the marble at the given coordinate.
     * @note It is precondition that figure in given coordinate exists.
     */
    color get_figure(coordinate c) const;

    /**
     * @brief Makes the given move by given coordinate and given direction.
     *        Also changes the turn to opposite color and if the count of
     *        lost marbles is >= 6, makes the game finished.
     * @note It is precondition that the move is correct.
     */
    void make_move(move m);

    /**
     * @brief Checks whether the given move is correct.
     */
    bool move_is_correct(move m) const;

    bool move_pushes(move m) const;

    bool move_pushes_and_destroys(move m) const;

    /**
     * @brief Checks whether the game is finished.
     */
    bool is_finished() const;

    /**
     * @brief Returns the coordinates of marbles of the given color.
     */
    std::vector<coordinate> get_figures(color c) const;

    /**
     * @brief Returns the color of current turn.
     */
    color get_turn() const;

    const std::vector<move>& get_history() const;

    color get_winner() const;

    int moves_count() const;

    void undo();

    void redo();

    bool has_undo_move() const;

    bool has_redo_move() const;

    int get_lost_count(color c) const;

private:
    void init_board();
    void create_figure(color c, coordinate cc);
    void set_coordinate(figure* f, coordinate c);
    void move_figure(coordinate c, direction d);
    void delete_figure(figure* f);
    figure* get_figure_int(coordinate c) const;
    figure* get_neighbour(figure* f, direction d) const;
    std::vector<coordinate> get_attackers(coordinate c, direction d) const;
    std::vector<coordinate> get_defenders(coordinate c, direction d, bool& m) const;
    void change_turn();
    void add_history(move m, int c);
    int make_move_int(move m);

private:
    std::vector<std::vector<figure*> > m_figures;
    std::vector<move> m_history;
    std::vector<int> m_moved_marbles_count;
    int m_lost[2];
    int m_current_position;
    color m_color;
    bool m_finished;
};

#endif
