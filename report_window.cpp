#include "report_window.hpp"
#include "engine.hpp"

#include <QtGui>

report_window::report_window(engine* e)
    : m_model(new QStandardItemModel(0, 4, this))
    , m_view(new QTreeView())
    , m_engine(e)
{
    m_model->setHeaderData(0, Qt::Horizontal, QObject::tr("ID"));
    m_model->setHeaderData(1, Qt::Horizontal, QObject::tr("Status"));
    m_model->setHeaderData(2, Qt::Horizontal, QObject::tr("Winner"));
    m_model->setHeaderData(3, Qt::Horizontal, QObject::tr("Moves"));

    m_view->setRootIsDecorated(false);
    m_view->setAlternatingRowColors(true);
    m_view->setModel(m_model);

    QGroupBox* b = new QGroupBox(tr("Games"));
    QHBoxLayout* l = new QHBoxLayout;
    l->addWidget(m_view);
    b->setLayout(l);

    QVBoxLayout* ml = new QVBoxLayout;
    ml->addWidget(b);

    setLayout(ml);
    setWindowTitle(tr("Games Report"));
    resize(500, 450);

    update_report();
}

void report_window::update_report()
{
    int s = m_model->rowCount();
    for (int j = 0; j < s; ++j) {
        m_model->removeRow(0);
    }
    int k = 0;
    int fsum = 0;
    int fmin = 0;
    int fmax = 0;
    int fc = 0;
    std::vector<int> g = m_engine->get_games();
    std::vector<int>::const_iterator i = g.begin();
    while(i != g.end()) {
        m_model->insertRow(k);
        m_model->setData(m_model->index(k, 0), QString::number(*i));
        const board& b = m_engine->get_board(*i);
        QString q;
        if (b.is_finished()) {
            ++fc;
            fsum += b.moves_count();
            if (fmin == 0 || fmin > b.moves_count()) {
                fmin = b.moves_count();
            }
            if (fmax < b.moves_count()) {
                fmax = b.moves_count();
            }
            q = "Finished";

        } else if (m_engine->is_game_stopped(*i)) {
            q = "Paused";
        } else {
            q = "Running";
        }
        m_model->setData(m_model->index(k, 1), q);
        q = "N/A";
        if (b.is_finished()) {
            if (b.get_winner() == BLACK_COLOR) {
                q = "Black";
            } else {
                q = "White";
            }
        }
        m_model->setData(m_model->index(k, 2), q);
        m_model->setData(m_model->index(k, 3), QString::number(b.moves_count()));
        for (int j = 0; j < 4; ++j) {
            QStandardItem* si = m_model->item(k, j);
            si->setFlags(si->flags() & (~Qt::ItemIsEditable));
        }
        ++k;
        ++i;
    }
    int favg = 0;
    if (fc != 0) {
        favg = fsum / fc;
    }
    m_model->insertRow(k);
    m_model->setData(m_model->index(k, 0), "Average");
    m_model->setData(m_model->index(k, 3), QString::number(favg));
    QStandardItem* si = m_model->item(k, 0);
    si->setFlags(si->flags() & (~Qt::ItemIsEditable));
    si = m_model->item(k, 3);
    si->setFlags(si->flags() & (~Qt::ItemIsEditable));

    m_model->insertRow(k + 1);
    m_model->setData(m_model->index(k + 1, 0), "Minimum");
    m_model->setData(m_model->index(k + 1, 3), QString::number(fmin));
    si = m_model->item(k + 1, 0);
    si->setFlags(si->flags() & (~Qt::ItemIsEditable));
    si = m_model->item(k + 1, 3);
    si->setFlags(si->flags() & (~Qt::ItemIsEditable));

    m_model->insertRow(k + 2);
    m_model->setData(m_model->index(k + 2, 0), "Maximum");
    m_model->setData(m_model->index(k + 2, 3), QString::number(fmax));
    si = m_model->item(k + 2, 0);
    si->setFlags(si->flags() & (~Qt::ItemIsEditable));
    si = m_model->item(k + 2, 3);
    si->setFlags(si->flags() & (~Qt::ItemIsEditable));
}
