#include "board.hpp"
#include "utility.hpp"

#include <cassert>

board::board()
    : m_figures()
    , m_history()
    , m_moved_marbles_count()
    , m_lost()
    , m_current_position(-1)
    , m_color(BLACK_COLOR)
    , m_finished(false)
{
    m_lost[0] = m_lost[1] = 0;
    init_board();
}

board::~board()
{
    for (int i = 0; i <= 8; ++i) {
        for (int j = 0; j < get_count_in_row(i + 1); ++j) {
            if (m_figures[i][j] != 0) {
                delete m_figures[i][j];
            }
        }
        m_figures[i].clear();
    }
    m_figures.clear();
}

bool board::figure_exists(coordinate c) const
{
    assert(check_coordinate(c));
    return get_figure_int(c) != 0;
}

color board::get_figure(coordinate c) const
{
    assert(check_coordinate(c));
    assert(figure_exists(c));
    return get_figure_int(c)->get_color();
}

void board::make_move(move m)
{
    int mc = make_move_int(m);
    add_history(m, mc);
}

bool board::move_is_correct(move m) const
{
    if (is_finished()) {
        return false;
    }
    coordinate cc = m.get_coordinate();
    direction dd = m.get_direction();
    figure* f = get_figure_int(cc);
    if (f == 0 || f->get_color() != m_color) {
        return false;
    }
    std::vector<coordinate> a = get_attackers(cc, dd);
    assert(!a.empty());
    if (m.get_move_type() == INLINE) {
        if (is_end(a.back(), dd)) {
            return false;
        }
        bool dummy = true;
        std::vector<coordinate> d = get_defenders(a.back(), dd, dummy);
        if (a.size() == d.size() && 
                a.size() == 3 && 
                m_lost[m_color] > m_lost[m_color == BLACK_COLOR ? WHITE_COLOR :
                                                    BLACK_COLOR]) {
            return true;
        }
        return dummy && a.size() > d.size() && a.size() <= 3;
    }
    int count = m.get_marbles_count();
    assert(count == 2 || count == 3);
    direction bd = get_broadside_direction(dd);
    if (static_cast<unsigned>(count) > a.size()) {
        return false;
    }
    for (int i = 0; i < count; ++i) {
        if (is_end(a[i], bd)) {
            return false;
        }
        figure* f = get_figure_int(a[i]);
        assert(f != 0);
        f = get_neighbour(f, bd);
        if (f != 0) {
            return false;
        }
    }
    return true;
}

bool board::move_pushes(move m) const
{
    if (!move_is_correct(m) || m.get_move_type() == BROADSIDE) {
        return false;
    }
    coordinate cc = m.get_coordinate();
    direction dd = m.get_direction();
    std::vector<coordinate> a = get_attackers(cc, dd);
    assert(!a.empty());
    bool dummy = true;
    std::vector<coordinate> d = get_defenders(a.back(), dd, dummy);
    return !d.empty();
}

bool board::move_pushes_and_destroys(move m) const
{
    if (!move_pushes(m)) {
        return false;
    }
    coordinate cc = m.get_coordinate();
    direction dd = m.get_direction();
    std::vector<coordinate> a = get_attackers(cc, dd);
    assert(!a.empty());
    bool dummy = true;
    std::vector<coordinate> d = get_defenders(a.back(), dd, dummy);
    return is_end(d.back(), dd);
}

bool board::is_finished() const
{
    return m_finished;
}

std::vector<coordinate> board::get_figures(color c) const
{
    std::vector<coordinate> r;
    std::vector<std::vector<figure*> >::const_iterator i = m_figures.begin();
    number x = 1;
    while (i != m_figures.end()) {
        std::vector<figure*>::const_iterator j = i->begin();
        number y = get_start_coordinate(static_cast<letter>(x));
        while (j != i->end()) {
            if (*j != 0 && (*j)->get_color() == c) {
                r.push_back(coordinate(static_cast<letter>(x), y));
            }
            ++j;
            ++y;
        }
        ++i;
        ++x;
    }
    return r;
}

color board::get_turn() const
{
    return m_color;
}

const std::vector<move>& board::get_history() const
{
    return m_history;
}

color board::get_winner() const
{
    assert(is_finished());
    if (m_lost[static_cast<int>(BLACK_COLOR)] >= 6) {
        return WHITE_COLOR;
    }
    return BLACK_COLOR;
}

int board::moves_count() const
{
    return m_current_position + 1;
}

void board::undo()
{
    assert(has_undo_move());
    move m = m_history[m_current_position];
    int mc = m_moved_marbles_count[m_current_position];
    coordinate c = m.get_coordinate();
    direction d = m.get_direction();
    if (m.get_move_type() == INLINE) {
        direction od = get_opposite_direction(d);
        c = ::get_neighbour(c, d);
        int i = 1;
        color l = get_figure(c);
        l = l == BLACK_COLOR ? WHITE_COLOR : BLACK_COLOR;
        for (; i <= mc; ++i) {
            move_figure(c, od);
            if (is_end(c, d)) {
                break;
            }
            c = ::get_neighbour(c, d);
        }
        assert(i >= mc - 1);
        if (i == mc - 1) {
            create_figure(l, c);
            --m_lost[l];
            if(m_lost[l] < 6) {
                m_finished = false;
            }
        }
    } else {
        direction bd = get_broadside_direction(d);
        direction od = get_opposite_direction(bd);
        for (int i = 0; i < mc; ++i) {
            move_figure(::get_neighbour(c, bd), od);
            if (is_end(c, d)) {
                break;
            }
            c = ::get_neighbour(c, d);
        }
    }
    --m_current_position;
    change_turn();
}

void board::redo()
{
    assert(has_redo_move());
    make_move_int(m_history[++m_current_position]);
}

bool board::has_undo_move() const
{
    return m_current_position > -1;
}

bool board::has_redo_move() const
{
    return m_current_position < static_cast<int>(m_history.size()) - 1;
}

int board::get_lost_count(color c) const
{
    return m_lost[c];
}

void board::init_board()
{
    for (int i = 0; i <= 8; ++i) {
        m_figures.push_back(std::vector<figure*>());
        for (int j = 0; j < get_count_in_row(i + 1); ++j) {
            m_figures[i].push_back(0);
        }
    }
    for (int i = 1; i <= 5; ++i) {
        create_figure(BLACK_COLOR, coordinate(A, i));
        create_figure(WHITE_COLOR, coordinate(I, i + 4));
    }
    for (int i = 1; i <= 6; ++i) {
        create_figure(BLACK_COLOR, coordinate(B, i));
        create_figure(WHITE_COLOR, coordinate(H, i + 3));
    }
    for (int i = 3; i <= 5; ++i) {
        create_figure(BLACK_COLOR, coordinate(C, i));
        create_figure(WHITE_COLOR, coordinate(G, i + 2));
    }
}

void board::create_figure(color c, coordinate cc)
{
    assert(check_coordinate(cc));
    m_figures[cc.first - 1][cc.second - get_start_coordinate(cc.first)] = new figure(c, cc);
}

void board::set_coordinate(figure* f, coordinate c)
{
    assert(m_figures[c.first - 1][c.second - get_start_coordinate(c.first)] == 0);
    m_figures[c.first - 1][c.second - get_start_coordinate(c.first)] = f;
    f->set_coordinate(c);
}

void board::move_figure(coordinate c, direction d)
{
    assert(check_coordinate(c));
    figure* f = m_figures[c.first - 1][c.second - get_start_coordinate(c.first)];
    if (f == 0) {
        return;
    }
    if (is_end(c, d)) {
        delete_figure(f);
        m_figures[c.first - 1][c.second - get_start_coordinate(c.first)] = 0;
        return;
    }
    switch (d) {
        case NORTH_EAST:
            set_coordinate(f, coordinate(static_cast<letter>(c.first + 1), c.second + 1));
            break;
        case NORTH_WEST:
            set_coordinate(f, coordinate(static_cast<letter>(c.first + 1), c.second));
            break;
        case WEST:
            set_coordinate(f, coordinate(c.first, c.second - 1));
            break;
        case SOUTH_WEST:
            set_coordinate(f, coordinate(static_cast<letter>(c.first - 1), c.second - 1));
            break;
        case SOUTH_EAST:
            set_coordinate(f, coordinate(static_cast<letter>(c.first - 1), c.second));
            break;
        case EAST:
            set_coordinate(f, coordinate(c.first, c.second + 1));
            break;
        default:
            assert(!"invalid direction!");
    }
    m_figures[c.first - 1][c.second - get_start_coordinate(c.first)] = 0;
}

void board::delete_figure(figure* f)
{
    ++m_lost[f->get_color()];
    coordinate c = f->get_coordinate();
    delete f;
    m_figures[c.first - 1][c.second - get_start_coordinate(c.first)] = 0;
}


figure* board::get_neighbour(figure* f, direction d) const
{
    coordinate c = f->get_coordinate();
    if (is_end(c, d)) {
        return 0;
    }
    c = ::get_neighbour(c, d);
    return m_figures[c.first - 1][c.second - get_start_coordinate(c.first)];
}

figure* board::get_figure_int(coordinate c) const
{
    return m_figures[c.first - 1][c.second - get_start_coordinate(c.first)];
}

std::vector<coordinate> board::get_attackers(coordinate c,
                                        direction d) const
{
    std::vector<coordinate> r;
    figure* f = m_figures[c.first - 1][c.second - get_start_coordinate(c.first)];
    assert(f != 0);
    r.push_back(f->get_coordinate());
    figure* g = 0;
    while((g = get_neighbour(f, d)) != 0 &&
            f->get_color() == g->get_color()) {
        f = g;
        r.push_back(f->get_coordinate());
    }
    return r;
}

std::vector<coordinate> board::get_defenders(coordinate c,
                                        direction d, bool& m) const
{
    std::vector<coordinate> r;
    figure* f = m_figures[c.first - 1][c.second - get_start_coordinate(c.first)];
    assert(f != 0);
    figure* g = get_neighbour(f, d);
    m = true;
    if (g != 0 && g->get_color() != f->get_color()) {
        r.push_back(g->get_coordinate());
        f = g;
        while((g = get_neighbour(f, d)) != 0) {
            if (f->get_color() != g->get_color()) {
                m = false;
                break;
            }
            f = g;
            r.push_back(f->get_coordinate());
        }
    }
    return r;
}

void board::change_turn()
{
    m_color = (m_color == BLACK_COLOR) ? WHITE_COLOR : BLACK_COLOR;
}

void board::add_history(move m, int mc)
{
    assert(m_history.size() == m_moved_marbles_count.size());
    int hs = static_cast<int>(m_history.size()) - 1;
    assert(m_current_position <= hs);
    if (m_current_position != hs) {
        std::vector<move>::iterator i = m_history.begin() + m_current_position + 1;
        m_history.erase(i, m_history.end());
        std::vector<int>::iterator j = m_moved_marbles_count.begin() + m_current_position + 1;
        m_moved_marbles_count.erase(j, m_moved_marbles_count.end());
    }
    assert(m_current_position == static_cast<int>(m_history.size()) - 1);
    m_history.push_back(m);
    m_moved_marbles_count.push_back(mc);
    ++m_current_position;
}

int board::make_move_int(move m)
{
    assert(move_is_correct(m));
    coordinate cc = m.get_coordinate();
    direction dd = m.get_direction();
    std::vector<coordinate> a = get_attackers(cc, dd);
    assert(!a.empty());
    int mc = 0;
    if (m.get_move_type() == INLINE) {
        assert(!is_end(a.back(), dd));
        bool m = true;
        std::vector<coordinate> d = get_defenders(a.back(), dd, m);
        assert(m == true);
        std::vector<coordinate>::reverse_iterator i = d.rbegin();
        while (i != d.rend()) {
            move_figure(*i++, dd);
        }
        i = a.rbegin();
        while (i != a.rend()) {
            move_figure(*i++, dd);
        }
        mc = a.size() + d.size();
    } else {
        int count = m.get_marbles_count();
        assert(count == 2 || count == 3);
        assert(static_cast<unsigned>(count) <= a.size());
        direction bd = get_broadside_direction(dd);
        for (int i = 0; i < count; ++i) {
            assert(!is_end(a[i], bd));
            move_figure(a[i], bd);
        }
        mc = count;
    }
    change_turn();
    if (m_lost[m_color] >= 6) {
        m_finished = true;
    }
    return mc;
}
