#include "main_window.hpp"
#include "board.hpp"
#include "engine.hpp"
#include "report_window.hpp"
#include "utility.hpp"

#include <QApplication>
#include <QFileDialog>
#include <QGraphicsItem>
#include <QMessageBox>
#include <QMenuBar>
#include <QPainter>
#include <QStatusBar>
#include <QString>
#include <QStringList>

main_window::main_window(engine* e)
    : QMainWindow()
    , m_engine(e)
    , m_canvas()
    , m_editor(&m_canvas, this)
    , m_current_game(-1)
    , m_random_level(new QAction("Random", this))
    , m_push_level(new QAction("Push", this))
    , m_push_and_destroy_level(new QAction("Push and Destroy", this))
    , m_sound(Phonon::createPlayer(Phonon::MusicCategory, Phonon::MediaSource("move.mp3")))
    , m_report_window(0)
{
    m_canvas.setSceneRect(0, 0, 600, 600);
    resize(700, 700);
    setWindowTitle(QString("Abalone"));
    statusBar();
    QMenuBar* menu = menuBar();
    QMenu* file = new QMenu("&File", menu);
    file->addAction("&New", this, SLOT(new_game()), Qt::CTRL+Qt::Key_N);
    file->addAction("&Save", this, SLOT(save()), Qt::CTRL+Qt::Key_S);
    file->addAction("&Open", this, SLOT(open()), Qt::CTRL+Qt::Key_O);
    file->addSeparator();
    file->addAction("E&xit", qApp, SLOT(quit()), Qt::CTRL+Qt::Key_Q);
    menu->addMenu(file);
    QMenu* edit = new QMenu("&Edit", menu);
    edit->addAction("&Pause", this, SLOT(pause()), Qt::CTRL+Qt::Key_P);
    edit->addAction("&Resume", this, SLOT(resume()), Qt::CTRL+Qt::Key_R);
    edit->addSeparator();
    edit->addAction("&Undo", this, SLOT(undo()), Qt::CTRL+Qt::Key_U);
    edit->addAction("R&edo", this, SLOT(redo()), Qt::CTRL+Qt::Key_E);
    edit->addSeparator();
    edit->addAction("Repor&t", this, SLOT(show_report_window()), Qt::CTRL+Qt::Key_T);
    menu->addMenu(edit);
    QMenu* level = new QMenu("&Level", menu);
    m_random_level->setCheckable(true);
    m_push_level->setCheckable(true);
    m_push_and_destroy_level->setCheckable(true);
    m_random_level->setChecked(true);
    connect(m_random_level, SIGNAL(triggered()), this, SLOT(set_random_level()));
    connect(m_push_level, SIGNAL(triggered()), this, SLOT(set_push_level()));
    connect(m_push_and_destroy_level, SIGNAL(triggered()), this, SLOT(set_push_and_destroy_level()));
    level->addAction(m_random_level);
    level->addAction(m_push_level);
    level->addAction(m_push_and_destroy_level);
    menu->addMenu(level);
    menu->addSeparator();
    QMenu* help = new QMenu("&Help", menu);
    help->addAction("&About", this, SLOT(help()), Qt::Key_F1);
    menu->addMenu(help);

    setCentralWidget(&m_editor);

    init();
}

main_window::~main_window()
{
    if (m_report_window != 0) {
        delete m_report_window;
        m_report_window = 0;
    }
}

void main_window::update_game(int id)
{
    if (m_current_game == id) {
        const board& b = m_engine->get_board(id);
        draw_board(b);
        m_sound->play();
    }
    if (m_report_window != 0) {
        m_report_window->update_report();
    }
}

void main_window::game_finished(int)
{
    if (m_report_window != 0) {
        m_report_window->update_report();
    }
}

void main_window::hideEvent(QHideEvent*)
{
    exit(0);
}

void main_window::init()
{
    clear();
    draw_hexagon();
    for (letter i = A; i <= I; i = static_cast<letter>(static_cast<int>(i) + 1)) {
        for (number x = get_start_coordinate(i); x <= get_end_coordinate(i); ++x) {
            draw_circle(coordinate(i, x));
        }
    }
}

void main_window::clear()
{
    m_editor.scene()->clear();
}

void main_window::help()
{
    static QMessageBox* about = new QMessageBox( "Abalone Game.",
	    "<h3>Abalone Board Game</h3>", QMessageBox::Information, 1, 0, 0, this, 0);
    about->setButtonText( 1, "Dismiss" );
    about->show();
}

void main_window::new_game()
{
    if (m_current_game >= 0) {
        m_engine->pause(m_current_game);
    }
    m_current_game = m_engine->create_new_game();
    update_game(m_current_game);
}

void main_window::save()
{
    if (m_current_game >= 0) {
        QFileDialog fd(this);
        fd.setAcceptMode(QFileDialog::AcceptSave);
        if (fd.exec()) {
            QStringList fl = fd.selectedFiles();
            std::string path = fl.front().toStdString();
            m_engine->save(m_current_game, path);
        }
    }
}

void main_window::open()
{
    QFileDialog fd(this);
    fd.setAcceptMode(QFileDialog::AcceptOpen);
    if (fd.exec()) {
        QStringList fl = fd.selectedFiles();
        std::string path = fl.front().toStdString();
        int i = m_engine->load(path);
        if (i >= 0) {
            if (m_current_game >= 0) {
                m_engine->pause(m_current_game);
            }
            m_current_game = i;
            update_game(m_current_game);
        } else {
            statusBar()->showMessage("Invalid file format.", 2);
        }
    }
}

void main_window::pause()
{
    if (m_current_game >= 0) {
        m_engine->pause(m_current_game);
        if (m_report_window != 0) {
            m_report_window->update_report();
        }
    }
}

void main_window::resume()
{
    if (m_current_game >= 0) {
        m_engine->resume(m_current_game);
    }
}

void main_window::undo()
{
    if (m_current_game >= 0) {
        m_engine->undo(m_current_game);
    }
}

void main_window::redo()
{
    if (m_current_game >= 0) {
        m_engine->redo(m_current_game);
    }
}

void main_window::set_random_level()
{
    m_engine->set_level(engine::RANDOM);
    m_random_level->setChecked(true);
    m_push_level->setChecked(false);
    m_push_and_destroy_level->setChecked(false);
}

void main_window::set_push_level()
{
    m_engine->set_level(engine::PUSH);
    m_random_level->setChecked(false);
    m_push_level->setChecked(true);
    m_push_and_destroy_level->setChecked(false);
}

void main_window::set_push_and_destroy_level()
{
    m_engine->set_level(engine::PUSH_AND_DESTROY);
    m_random_level->setChecked(false);
    m_push_level->setChecked(false);
    m_push_and_destroy_level->setChecked(true);
}

void main_window::show_report_window()
{
    if (m_report_window == 0) {
        m_report_window = new report_window(m_engine);
    }
    m_report_window->show();
}

void main_window::draw_board(const board& b)
{
    init();
    std::vector<coordinate> v = b.get_figures(WHITE_COLOR);
    std::vector<coordinate>::const_iterator i = v.begin();
    while (i != v.end()) {
        draw_circle(*i, WHITE_COLOR);
        ++i;
    }
    v = b.get_figures(BLACK_COLOR);
    i = v.begin();
    while (i != v.end()) {
        draw_circle(*i, BLACK_COLOR);
        ++i;
    }
    draw_lost_marbles(b.get_lost_count(BLACK_COLOR), BLACK_COLOR);
    draw_lost_marbles(b.get_lost_count(WHITE_COLOR), WHITE_COLOR);
}

void main_window::draw_circle(coordinate c, color cc)
{
    int y = static_cast<int>(c.first) - 1;
    int x = c.second - 1;
    const int width = 300;
    QAbstractGraphicsShapeItem* i = m_canvas.addEllipse(QRectF(0,0,50,50));
    i->setPen(Qt::NoPen);
    QColor col = cc == WHITE_COLOR ? QColor(255, 255, 255) : QColor(0, 0, 0);
    i->setBrush(col);
    i->setPos(m_canvas.width() / 2 - width / 2 + 5 + x * 60 - y * 60 / 2, m_canvas.height() - 75 - y * 60 * 173 / 100 / 2);
}

void main_window::draw_circle(coordinate c)
{
    int y = static_cast<int>(c.first) - 1;
    int x = c.second - 1;
    const int width = 300;
    QAbstractGraphicsShapeItem* i = m_canvas.addEllipse(QRectF(0,0,50,50));
    i->setPen(Qt::NoPen);
    i->setBrush(QColor(255, 0, 0));
    i->setPos(m_canvas.width() / 2 - width / 2 + 5 + x * 60 - y * 60 / 2, m_canvas.height() - 75 - y * 60 * 173 / 100 / 2);
}

void main_window::draw_hexagon()
{
    const int width = 300;
    QPolygon polygon;
    polygon << QPoint(-width / 2, 0)
            << QPoint(-width, -width * 173 / 100 / 2)
            << QPoint(-width / 2, -width * 173 / 100)
            << QPoint(width / 2, -width * 173 / 100)
            << QPoint(width, -width * 173 / 100 / 2)
            << QPoint(width / 2, 0);

    QGraphicsPolygonItem* i = m_canvas.addPolygon(polygon);
    i->setPen(Qt::NoPen);
    i->setBrush(QColor(160, 0, 0));
    i->setPos(m_canvas.width() / 2, m_canvas.height());
}

void main_window::draw_lost_marbles(int c, color cc)
{
    int y = cc == BLACK_COLOR ? 25 : -35;
    int x = -35;
    QPen p(QColor(0, 0, 0));
    QColor col = cc == WHITE_COLOR ? QColor(255, 255, 255) : QColor(0, 0, 0);
    for (int j = 0; j < c; ++j) {
        QAbstractGraphicsShapeItem* i = m_canvas.addEllipse(QRectF(0,0,50,50));
        i->setPen(p);
        i->setBrush(col);
        i->setPos(x, y);
        x += 60;
    }
}
