#ifndef CANVAS_HPP
#define CANVAS_HPP

#include "basic_types.hpp"

#include <Phonon/MediaObject>
#include <QHideEvent>
#include <QMenu>
#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsScene>

class engine;
class board;
class report_window;

/**
 * @class main_window.
 * @brief GUI window, for showing game board.
 */
class main_window : public QMainWindow
{
    Q_OBJECT
public:
    main_window(engine* e);
    ~main_window();

public slots:
    /// @brief Updates the board by the game with given id.
    void update_game(int);
    void game_finished(int);

protected:
    void hideEvent(QHideEvent*);

private slots:
    void help();
    void new_game();
    void save();
    void open();
    void pause();
    void resume();
    void undo();
    void redo();
    void set_random_level();
    void set_push_level();
    void set_push_and_destroy_level();
    void show_report_window();

private:
    void clear();
    void init();

    void draw_board(const board& b);

    void draw_circle(coordinate c);
    void draw_circle(coordinate c, color cc);
    void draw_hexagon();
    void draw_lost_marbles(int c, color cc);

private:
    engine* m_engine;
    QGraphicsScene m_canvas;
    QGraphicsView m_editor;
    int m_current_game;
    QAction* m_random_level;
    QAction* m_push_level;
    QAction* m_push_and_destroy_level;
    Phonon::MediaObject* m_sound;
    report_window* m_report_window;
};

#endif
