#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "board.hpp"

#include <QObject>
#include <QTimer>

#include <map>
#include <set>
#include <vector>

class main_window;

/**
 * @class engine.
 * @brief engine class for creating and managing the games.
 *
 *        Engine is singleton class which is responsible for game
 *        management. It provides interface to create new game and
 *        automatically run it. Each newly created game has its
 *        own id and users can access to the game info (board,
 *        statistics) by that id.
 *        After creating game it is automatically running and after
 *        each 2 sec move is done in game and GUI is updated
 *        accordingly.
 */
class engine : public QObject
{
    Q_OBJECT
public:
    engine();
    ~engine();

public:
    enum level {
        RANDOM,
        PUSH,
        PUSH_AND_DESTROY
    };

public:
    /// @brief Access to GUI window.
    main_window& get_main_window();

    /// @brief Creates new game, runs it and returns id of game.
    int create_new_game();

    /// @brief Checks whether the game with given id exists.
    bool game_exists(int id) const;

    /// @brief Access to the board of the given game.
    /// @note It is precondition that game with given id exists.
    const board& get_board(int id) const;
    board& get_board(int id);

    void pause(int id);
    void resume(int id);
    void undo(int id);
    void redo(int id);

    bool is_game_stopped(int id) const;

    void save(int id, std::string path) const;
    int load(std::string path);

    level get_level() const;
    void set_level(level l);

    std::vector<int> get_games() const;

private slots:
    void update_games();

signals:
    void game_updated(int);
    void game_finished(int);

private:
    void make_random_move(board& b);

private:
    main_window* m_main_window;
    std::map<int, board*> m_games;
    std::set<int> m_stopped_games;
    int m_max_id;
    QTimer m_timer;
    level m_level;
};

#endif
