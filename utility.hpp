#ifndef UTILITY_HPP
#define UTILITY_HPP

#include "basic_types.hpp"

/**
 * @brief Checks whether the coordinate is correct.
 */
bool check_coordinate(coordinate c);

/**
 * @brief Returns the count of cells in a given row.
 */
int get_count_in_row(int c);

/**
 * @brief Returns the start position of the given row.
 */
number get_start_coordinate(letter l);

/**
 * @brief Returns the end position of the given row.
 */
number get_end_coordinate(letter l);

/**
 * @brief Checks whether the given coordinate is the last for
 *        the given direction.
 */
bool is_end(coordinate c, direction d);

/**
 * @brief Returns the coordinate of the neighbour of the
 *        given coordinate by the given direction.
 * @note It is precondition the the given coordinate in given
 *       direction is not the last.
 */
coordinate get_neighbour(coordinate c, direction d);

direction get_broadside_direction(direction d);

direction get_opposite_direction(direction d);

#endif
