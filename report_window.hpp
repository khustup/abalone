#ifndef REPORT_WINDOW_HPP
#define REPORT_WINDOW_HPP

#include <QWidget>

class QLabel;
class QStandardItemModel;
class QTreeView;

class engine;

class report_window : public QWidget
{
    Q_OBJECT

public:
    report_window(engine*);

public:
    void update_report();

private:
    QStandardItemModel* m_model;
    QTreeView* m_view;
    engine* m_engine;
};

#endif
